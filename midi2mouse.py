#!/usr/bin/env python3 
"""I'm just tired of the fucking mouse.
Let's use a MIDI controller to, well, 'control' the screen pointer."""

import os
import mido
import sys


ports = mido.get_input_names()
sys.stdout.write(f'Available ports: { ports }\n')

supported_device = 'nanoPAD2'
device = ''
for port in ports:
    if port.startswith(supported_device):
        device = port

if not device:
    sys.exit('No compatible device found')

'''
The nanoPAD2 coordinates
127
|
|
|
|________
0        127 

And my scree is 

0________________1920
|
|
|
|
1080

So...

Let's create a function that takes a nanopad coordinate and return a sreen coordinate.
'''

start_x, start_y = 0, 0
distance_x, distance_y = 0, 0

with mido.open_input(device) as input_port:
    for msg in input_port:
        if msg.type == 'control_change':
            if msg.control == 16:
                start_x = 0
                start_y = 0
                distance_x = 0 
                distance_y = 0
                continue
            if msg.control == 1:
                if start_x == 0:
                    start_x = msg.value
                    continue
                distance_x = msg.value - start_x
                start_x = msg.value
            elif msg.control == 2:
                if start_y == 0:
                    start_y = msg.value
                    continue
                distance_y = start_y - msg.value
                start_y = msg.value
        elif msg.type == 'note_on':
            if msg.note == 36:
                os.system('xdotool mousedown 1')
                os.system('xdotool mouseup 1')
            elif msg.note == 38:
                os.system('xdotool mousedown 3')
                os.system('xdotool mouseup 3')
            elif msg.note == 35:
                # Hold to scroll...
                pass
            elif msg.note == 37:
                os.system('xdotool set_desktop 0')
            elif msg.note == 39:
                os.system('xdotool set_desktop 1')
            elif msg.note == 41:
                os.system('xdotool set_desktop 2')
            elif msg.note == 43:
                os.system('xdotool set_desktop 3')
            elif msg.note == 45:
                os.system('xdotool set_desktop 4')
            elif msg.note == 47:
                os.system('xdotool set_desktop 5')
            elif msg.note == 49:
                os.system('xdotool set_desktop 6')
            elif msg.note == 51:
                os.system('xdotool set_desktop 7')
        elif msg.type == 'note_off':
            if msg.note == 36:
                os.system('xdotool mouseup 1')
            elif msg.note == 38:
                os.system('xdotool mouseup 3')
        
        if distance_x or distance_y:
            os.system(f'xdotool mousemove_relative -- { distance_x * 5 } { distance_y * 3 }')



